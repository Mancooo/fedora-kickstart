#version=DEVEL
# System authorization information
authselect --enableshadow --passalgo=sha512

# Use graphical install
graphical

# Configure installation method
url --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-29&arch=x86_64"
repo --name=fedora-updates --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f29&arch=x86_64" --cost=0
repo --name=rpmfusion-free --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-29&arch=x86_64" --includepkgs=rpmfusion-free-release
repo --name=rpmfusion-free-updates --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-updates-released-29&arch=x86_64" --cost=0

# Keyboard layouts
keyboard --vckeymap=us-euro --xlayouts='us (euro)'

# Configure X Window System
xconfig --startxonboot

# System services
services --disabled="mdmonitor,multipathd,dnf-makecache.timer,firewalld,sshd,switcheroo-control,NetworkManager-wait-online,wpa_supplicant"
services --enabled="fstrim.timer,dbus-broker,gdm"

# Network information
network  --hostname=desktop_pc

# System timezone
timezone Europe/Amsterdam --isUtc

# System bootloader configuration
bootloader --location=mbr --boot-drive=sda --append="i915.fastboot=1 rhgb quiet audit=0 nmi_watchdog=0 nowatchdog vt.global_cursor_default=0 ipv6.disable=1 threadirqs plymouth.splash-delay=20"

%packages
@^minimal-environment
-plymouth*
@base-x
-xorg-x11-drv-armsoc
-xorg-x11-drv-ati
-xorg-x11-drv-evdev
-xorg-x11-drv-fbdev
-xorg-x11-drv-intel
xorg-x11-drv-libinput
-xorg-x11-drv-nouveau
-xorg-x11-drv-omap
-xorg-x11-drv-openchrome
-xorg-x11-drv-qxl
-xorg-x11-drv-vesa
-xorg-x11-drv-vmware
-xorg-x11-drv-wacom
### System tools
bash-completion 
sqlite
nano
pciutils
smartmontools
msr-tools
htop
tlp
dbus-broker
### Gnome and GUI tools & apps
gnome-shell
firefox
tilix
eog
gnome-calculator
alsa-plugins-pulseaudio
gnome-tweaks
dconf-editor
gnome-disk-utility
file-roller
youtube-dl
evince
gedit
nautilus
xdg-user-dirs
xdg-user-dirs-gtk
gvfs-mtp
libreoffice-writer
libreoffice-langpack-nl
gimp
keepassxc
#fwupd
microcode_ctl
streamlink
gnome-terminal
tilix-nautilus
#crda
mpv
ffmpeg
libva-intel-driver
libva-intel-hybrid-driver
avahi
orca
nss-mdns
ibus-typing-booster
gnome-user-share
pulseaudio-utils
ufw
net-tools
transmission-gtk
NetworkManager-openvpn
### Fonts
google-roboto-fonts
google-roboto-mono-fonts
google-roboto-slab-fonts
google-roboto-condensed-fonts
dejavu-sans-mono-fonts                                                                              
dejavu-serif-fonts
liberation-mono-fonts
liberation-sans-fonts
liberation-serif-fonts

%end

%post
# Any post ops, just add the bash script commands here.
systemctl disable firewalld.service
systemctl mask systemd-rfkill.service
systemctl mask systemd-rfkill.socket
systemctl --global enable dbus-broker.service
dnf -y copr enable daniruiz/flat-remix
dnf -y install flat-remix
echo "echo \"options iwlwifi led_mode=1\" >> /etc/modprobe.d/iwlwifi.conf" | bash
echo "echo \"blacklist iTCO_wdt\" >> /etc/modprobe.d/blacklist.conf" | bash
cp /etc/sysctl.conf /etc/sysctl.conf.bak
cp /etc/default/tlp /etc/default/tlp.bak
cp /etc/fstab /etc/fstab.bak
cp /etc/gdm/custom.conf /etc/gdm/custom.conf.bak
cp /etc/dnf/dnf.conf /etc/dnf/dnf.conf.bak
cp /etc/pulse/default.pa /etc/pulse/default.pa.bak
cp /etc/pulse/daemon.conf /etc/pulse/daemon.conf.bak
cp /etc/pulse/system.pa /etc/pulse/system.pa.bak
cp /etc/security/limits.d/95-jack.conf /etc/security/limits.d/95-jack.conf.bak
echo "echo \"vm.swappiness=10\" >> /etc/sysctl.conf" | bash
echo "echo \"#disable ipv6\" >> /etc/sysctl.conf" | bash
echo "echo \"net.ipv6.conf.all.disable_ipv6 = 1\" >> /etc/sysctl.conf" | bash
echo "echo \"net.ipv6.conf.default.disable_ipv6 = 1\" >> /etc/sysctl.conf" | bash
echo "echo \"net.ipv6.conf.lo.disable_ipv6 = 1\" >> /etc/sysctl.conf" | bash
echo "echo \"net.ipv4.ip_forward = 1\" >> /etc/sysctl.conf" | bash

%end

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
%end
