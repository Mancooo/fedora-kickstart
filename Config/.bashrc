# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
# alias ls='LANG=C ls --color -h --group-directories-first'
# alias ls='LANG=C ls -lAh --color -h --group-directories-first'
alias ls='LANG=C ls -lhGA --color -h --group-directories-first'
alias find='find -iname'
alias cp='cp -i'
alias rm='rm -Iv'
alias du='du -bhsc --time'
alias dus='du .[!.]* *'
alias u='sudo dnf check-update --refresh'
alias s='startx >/dev/null 2>&1'
alias w='env XDG_SESSION_TYPE=wayland gnome-session'
alias df='df -hT -x devtmpfs -x tmpfs --total --sync'
alias mpa='mpv --no-video'
alias smpv='mpv --profile=highquality'

# Televizyon kanallari
alias ntv='streamlink https://www.youtube.com/watch?v=JZ8QhI0pz1A'
alias kanald='streamlink https://www.kanald.com.tr/canli-yayin'
alias cnnturk='streamlink https://www.youtube.com/watch?v=AcH2okF7bhA'
alias haberturk='streamlink https://www.youtube.com/watch?v=iYY4F4EbWLc'
alias superfm='mpv http://17703.live.streamtheworld.com/SUPER_FM128AAC?'